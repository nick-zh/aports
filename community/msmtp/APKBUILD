# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=msmtp
pkgver=1.8.17
pkgrel=0
pkgdesc="SMTP client with a sendmail compatible interface"
url="https://marlam.de/msmtp/"
arch="all"
license="GPL-3.0-or-later"
options="!check" # no test suite
makedepends="gettext-dev gnutls-dev libidn2-dev"
subpackages="$pkgname-doc $pkgname-lang $pkgname-openrc $pkgname-vim:vim:noarch"
source="https://marlam.de/msmtp/releases/msmtp-$pkgver.tar.xz
	fix-intl.patch
	msmtp.confd
	msmtp.initd"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

package() {
	make DESTDIR="$pkgdir" install

	install -Dm755 "$srcdir/$pkgname.initd" "$pkgdir/etc/init.d/$pkgname"
	install -Dm644 "$srcdir/$pkgname.confd" "$pkgdir/etc/conf.d/$pkgname"

	mkdir -p "$pkgdir"/usr/share/doc/$pkgname
	install -m644 README NEWS "$pkgdir"/usr/share/doc/$pkgname
}

vim() {
	depends=""
	pkgdesc="Vim syntax for $pkgname"

	install -Dm644 "$builddir"/scripts/vim/$pkgname.vim \
		"$subpkgdir"/usr/share/vim/vimfiles/syntax/$pkgname.vim
}

sha512sums="
a6f8f30af47e432ffaef5b47b105f6b4ea0a6ca3069689137c66112b6425eca8775ad4f7606c3ca0dfc132c14c6f9969767329615a82d1f5a11f07dfdf151ced  msmtp-1.8.17.tar.xz
3839d7e8400cf897830c674f59563e225096d9755121d1ac14f3b493ab4cc0672228a9b14b9d9620d566593e2ae27d322a78c46791c6b5166ab82275d25dc0d3  msmtp.confd
0e1b32b07ccac6f5a174a1e317390815b459a4a7ca4f15672456ac0a30c89edb93001a8047c38d1f8da65b9f07a7b17b3270a0a07248a6be248500cd42773cef  msmtp.initd
7f7395225fee67a3a56c3a136f23197bb222f07647426582c4f434699bc5efb89831b7b075d82bc156225994e1950bb63d55730110d37869501e77b2abf586bc  fix-intl.patch
"
